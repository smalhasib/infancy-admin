package com.infancyit.springbackend.data;

import com.infancyit.springbackend.domain.Role;
import com.infancyit.springbackend.domain.User;
import com.infancyit.springbackend.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserSeeder {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @EventListener(ApplicationReadyEvent.class)
    public void seedDatabaseWhenApplicationReady() {
        var user = userRepository.findByUsername("infancyit");
        if (user.isEmpty()) {
            var newUser = User.builder()
                    .username("infancyit")
                    .email("admin@infancyit.com")
                    .password(passwordEncoder.encode("password"))
                    .role(Role.ADMIN)
                    .build();
            userRepository.save(newUser);
        }
    }
}
