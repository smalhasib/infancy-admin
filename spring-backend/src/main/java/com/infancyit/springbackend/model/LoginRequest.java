package com.infancyit.springbackend.model;

public record LoginRequest(String username, String password) {
}
