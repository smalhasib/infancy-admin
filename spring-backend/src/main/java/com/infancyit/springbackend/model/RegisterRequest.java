package com.infancyit.springbackend.model;

public record RegisterRequest(String username, String email, String password) {
}
