package com.infancyit.springbackend.scheduler;

import com.infancyit.springbackend.repository.TokenRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DBOperationScheduler {

    private final TokenRepository tokenRepository;

    @Scheduled(fixedRate = 30 * 3600)
    @Transactional
    public void clearExpiredAndRevokedTokens() {
        tokenRepository.deleteTokensByExpiredIsTrueAndRevokedIsTrue();
    }
}
